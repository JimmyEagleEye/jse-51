package ru.korkmasov.tsc.command.domain;

import ru.korkmasov.tsc.command.AbstractCommand;
import ru.korkmasov.tsc.exception.empty.EmptyDomainException;
import ru.korkmasov.tsc.enumerated.Role;
import ru.korkmasov.tsc.dto.Domain;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractDomainCommand extends AbstractCommand {

    @NotNull
    protected static final String BACKUP_XML = "./backup.xml";
    @NotNull
    protected static final String FILE_BINARY = "./data.bin";
    @NotNull
    protected static final String FILE_BASE64 = "./data.base64";
    @NotNull
    protected static final String FILE_FASTERXML_JSON = "./data-fasterxml.json";
    @NotNull
    protected static final String FILE_FASTERXML_XML = "./data-fasterxml.xml";
    @NotNull
    protected static final String FILE_FASTERXML_YAML = "./data-fasterxml.yaml";
    @NotNull
    protected static final String FILE_JAXB_JSON = "./data-jaxb.json";
    @NotNull
    protected static final String FILE_JAXB_XML = "./data-jaxb.xml";
    @NotNull
    protected static final String JAXB_JSON_PROPERTY_NAME = "eclipselink.media-type";
    @NotNull
    protected static final String JAXB_JSON_PROPERTY_VALUE = "application/json";
    @NotNull
    protected static final String SYSTEM_JSON_PROPERTY_NAME = "javax.xml.bind.context.factory";
    @NotNull
    protected static final String SYSTEM_JSON_PROPERTY_VALUE = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        /*domain.setUsers(serviceLocator.getUserService().findAll());
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());*/
        return domain;
    }

    public void setDomain(@Nullable Domain domain) {
        if (domain == null) throw new EmptyDomainException();
        /*serviceLocator.getTaskService().clear();
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getProjectService().clear();
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
        serviceLocator.getAuthService().logout();*/
    }
}
