package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.endpoint.Task;
import ru.korkmasov.tsc.command.task.AbstractTaskCommand;
import ru.korkmasov.tsc.util.TerminalUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create task";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @Nullable final String description = TerminalUtil.nextLine();
        //@Nullable final Task task = add(name, description);
        //serviceLocator.getTaskEndpoint().addTask(serviceLocator.getSession(), task);
    }

}
