package ru.korkmasov.tsc.command.project;

import ru.korkmasov.tsc.enumerated.Sort;
import ru.korkmasov.tsc.endpoint.Project;
import ru.korkmasov.tsc.util.TerminalUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;

public class ProjectListCommand extends AbstractProjectCommand {
    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-list";
    }

    @Override
    public @NotNull String description() {
        return "Show project list";
    }

    @Override
    public void execute() {
        System.out.println("Enter sort");
        @Nullable List<Project> projects = serviceLocator.getProjectEndpoint().findProjectAll(serviceLocator.getSession());
        int index = 1;
        for (@NotNull final Project project : projects) {
            System.out.println(index + ". " + toString(project));
            index++;
        }
    }

}
