package ru.korkmasov.tsc.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.exception.empty.EmptyEmailException;
import ru.korkmasov.tsc.exception.user.EmailExistsException;
import ru.korkmasov.tsc.exception.user.LoginExistsException;
import ru.korkmasov.tsc.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-registry";
    }

    @Override
    public String description() {
        return "Register new user";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        //if (serviceLocator.getAdminEndpoint().existsUserByLogin(login)) throw new LoginExistsException();
        System.out.println("ENTER E-MAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        //if (isEmpty(email)) throw new EmptyEmailException();
        //if (serviceLocator.getAdminEndpoint().existsUserByEmail(email)) throw new EmailExistsException();
        System.out.println("ENTER PASSWORD:");
        //serviceLocator.getAdminEndpoint().registryUser(login, TerminalUtil.nextLine(), email);
    }

}