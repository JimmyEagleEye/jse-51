package ru.korkmasov.tsc.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.util.TerminalUtil;

public class UserUpdateCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-update";
    }

    @Override
    public String description() {
        return "Update user profile";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROFILE]");
        System.out.println("ENTER FIRST NAME:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        //serviceLocator.getAdminEndpoint().updateUser(serviceLocator.getSession(), firstName, lastName, middleName);
    }

}