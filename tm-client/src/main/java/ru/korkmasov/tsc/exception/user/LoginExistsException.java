package ru.korkmasov.tsc.exception.user;

import ru.korkmasov.tsc.exception.AbstractException;

public class LoginExistsException extends AbstractException {

    public LoginExistsException() {
        super("Error! Login already exists...");
    }

}
