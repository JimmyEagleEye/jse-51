package ru.korkmasov.tsc.model;

import ru.korkmasov.tsc.api.entity.ITWBS;
import ru.korkmasov.tsc.enumerated.Status;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Project extends AbstractOwner implements ITWBS {

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @Nullable
    private Status status = Status.NOT_STARTED;

    public Project(@Nullable final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return getId() + " :" + name;
    }

    @Nullable
    private Date created = new Date();

    public void setStatus(@Nullable final Status status) {
        this.status = status;
        if (status == null) return;
        switch (status) {
            case IN_PROGRESS:
                this.setDateStart(new Date());
                break;
            case COMPLETE:
                this.setDateFinish(new Date());
            default:
                break;
        }
    }

}
