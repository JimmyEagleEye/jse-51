package ru.korkmasov.tsc.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.endpoint.*;
import ru.korkmasov.tsc.model.Session;

public interface EndpointLocator {

    @NotNull
    AdminEndpoint getAdminEndpoint();

    @NotNull
    AdminUserEndpoint getAdminUserEndpoint();

    @NotNull ProjectEndpoint getProjectEndpoint();

    @NotNull IPropertyService getPropertyService();

    @Nullable Session getSession();

    void setSession(@Nullable Session session);

    @NotNull SessionEndpoint getSessionEndpoint();

    @NotNull TaskEndpoint getTaskEndpoint();

    @NotNull UserEndpoint getUserEndpoint();

}
