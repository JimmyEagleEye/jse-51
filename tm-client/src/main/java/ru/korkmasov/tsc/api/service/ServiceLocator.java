package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.endpoint.*;
import ru.korkmasov.tsc.api.service.IPropertyService;
import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.endpoint.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;



public interface ServiceLocator {

    //@NotNull ITaskService getTaskService();

    //@NotNull IProjectService getProjectService();

    //@NotNull IProjectTaskService getProjectTaskService();

    @NotNull ICommandService getCommandService();

    //@NotNull IAuthService getAuthService();

    //@NotNull IUserService getUserService();

    @NotNull IPropertyService getPropertyService();

    //@NotNull ISessionService getSessionService();

    void setSession(Session session);

    @NotNull TaskEndpoint getTaskEndpoint();

    @NotNull ProjectEndpoint getProjectEndpoint();

    @NotNull SessionEndpoint getSessionEndpoint();

    @NotNull AdminEndpoint getAdminEndpoint();

    @NotNull DataEndpoint getDataEndpoint();

    @Nullable Session getSession();

}
