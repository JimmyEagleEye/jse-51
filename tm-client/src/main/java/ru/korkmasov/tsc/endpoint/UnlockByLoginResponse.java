
package ru.korkmasov.tsc.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "unlockByLoginResponse", propOrder = {
    "_return"
})
public class UnlockByLoginResponse {

    @XmlElement(name = "return")
    protected UserEndpoint _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link UserEndpoint }
     *     
     */
    public UserEndpoint getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserEndpoint }
     *     
     */
    public void setReturn(UserEndpoint value) {
        this._return = value;
    }

}
