package ru.korkmasov.tsc.bootstrap;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.api.service.IReceiverService;
import ru.korkmasov.tsc.listener.LogMessageListener;
import ru.korkmasov.tsc.service.ReceiverService;

import static ru.korkmasov.tsc.constant.ActiveMQConst.URL;

public final class Bootstrap {

    public void start() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(URL);
        factory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new ReceiverService(factory);
        receiverService.receive(new LogMessageListener());
    }

}
