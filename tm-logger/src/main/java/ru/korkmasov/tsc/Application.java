package ru.korkmasov.tsc;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.bootstrap.Bootstrap;


public class Application {

    @SneakyThrows
    public static void main(final String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}