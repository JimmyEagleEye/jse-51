package ru.korkmasov.tsc.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.api.service.IReceiverService;

import javax.jms.*;

import static ru.korkmasov.tsc.constant.ActiveMQConst.STRING;

public class ReceiverService implements IReceiverService {

    @NotNull
    private final ConnectionFactory connectionFactory;

    public ReceiverService(@NotNull ConnectionFactory factory) {
        connectionFactory = factory;
    }

    @Override
    @SneakyThrows
    public void receive(@NotNull final MessageListener listener) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();

        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic(STRING);
        final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(listener);
    }

}
