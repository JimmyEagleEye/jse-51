package ru.korkmasov.tsc.exception.entity;

import ru.korkmasov.tsc.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error. ProjectDTO not found.");
    }

}
