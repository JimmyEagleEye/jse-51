package ru.korkmasov.tsc.exception.empty;

import ru.korkmasov.tsc.exception.AbstractException;

public class EmptyIndexException extends AbstractException {

    public EmptyIndexException() {
        super("Error. Index is empty");
    }

}

