package ru.korkmasov.tsc.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.api.service.IMessageService;
import ru.korkmasov.tsc.dto.LoggerDTO;
import ru.korkmasov.tsc.service.MessageService;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MessageExecutor {

    @NotNull
    private static final int THREAD_COUNT = 3;

    @NotNull
    private final IMessageService service = new MessageService();

    @NotNull
    private final ExecutorService es = Executors.newFixedThreadPool(THREAD_COUNT);

    public void sendMessage(@Nullable final Object record,
                            @NotNull final String type) {
        es.submit(() -> {
            @NotNull final LoggerDTO entity = service.prepareMessage(record, type);
            service.sendMessage(entity);
        });
    }

    public void stop() {
        es.shutdown();
    }

}
